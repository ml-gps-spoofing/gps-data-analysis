## Machine Learning System for GPS Spoofing Detection

The objective of this project is to build a the prototype of a system that is able to reveal the spoofing of a GPS signal through the use of Machine Learning algorithms.